$(function(){
	// jquery knob plugin
	$(".dial").knob();
	
	// scrollspy
	$("body").scrollspy({ 
		target: "#main-nav" 
	});
	
	// init accordion status style
	$('.click').addClass('collapsed').on('click', function () {
		if( $(this).hasClass('collapsed') !== true){
			$(this).removeClass('active');
		}else{
			$(this).addClass('active');
		}
	});
	
	// jquery scrollto plugin
	$("nav").localScroll();
	$("#about").localScroll();
	
	// jquery mixitup plugin
	$("#grid").mixitup();
	

});